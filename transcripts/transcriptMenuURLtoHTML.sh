#!/bin/bash
# example usage:
# ./transcriptMenuURLtoHTML.sh https://www.llresearch.org/transcripts/indexes/2010.aspx

url=$1

wget $url

menufile=$(echo $url | sed -e 's/.*\///')
echo "menufile is $menufile"

urlsuffixes=$(cat $menufile| grep -o issues/..../...._.....aspx)

echo "" > $menufile.html
for suffix in $urlsuffixes
do
  wget https://www.llresearch.org/transcripts/$suffix
  file=$(echo $suffix| sed -e 's/.*\///')
  echo "processing $file"
  cat $file | awk '/transcript-title/ { found = 1}; 
  /separator-left/ { found = 0}; /class="nav"/ { found = 0 }; 
  found { print }' >> $menufile.html
done 
