#!/usr/bin/env node
"use strict";
const fs = require("fs");
const path = require("path");
fs.readdir("./", (err, files) => {
	let folders = files.filter((file) => {
		return path.extname(file) == ""? true: false;
	});
	let folder = folders[Math.round(Math.random()*(folders.length))];
	fs.readdir(`./${folder}`, "utf8", (err, files2) => {
		let file =files2[Math.round(Math.random()*(files2.length))];
		fs.readFile(`./${folder}/${file}`, (err, data) => {
			let lines = data.toString().split('\n');
			let lineNumber = Math.round(Math.random()*(lines.length));
			let line =  lines[lineNumber];
			let month = file.substring(0,2);
			let day = file.substring(2,4);
			console.log(`"${line}"\n LLResearch ${folder}/${month}/${day}:${lineNumber}`)
		});
	} );
} );
