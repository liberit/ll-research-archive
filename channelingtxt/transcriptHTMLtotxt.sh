#!/bin/bash
for dir in ls -d */
do
	for file in $(ls $dir)
	do
		echo "$dir/$file"
		cat "$dir/$file" | grep 'dialogue\|speaker\|notes'|\
			sed -e 's/<[^>]*>//g;s/\[new speaker\]//'>\
			"$dir/$file.txt"
	done
done

