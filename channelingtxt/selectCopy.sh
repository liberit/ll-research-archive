#!/bin/bash
question=$1
destination=$2

for i in $(grep -inrl "$question" *)
do
	echo $i
	directory=$(echo $i|sed -e 's/\/.*//')
	mkdir $destination/$directory
	cp $i $destination/$directory
done
