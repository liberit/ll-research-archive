# LL Research Archives

A printed copy is of them is more valueable than its weight in diamonds. 

The file which is printable is
[llresearch-1972-2020.txt.ps](https://gitlab.com/liberit/ll-research-archive/-/raw/master/llresearch-1972-2020.txt.ps)

The searchable epub (eBook format) (1972-2021) is:
[llresearch.epub](https://gitlab.com/liberit/ll-research-archive/-/raw/master/channelingtxt/llresearch.epub)

The plain text is:
[llresearch-1972-2020.txt](https://gitlab.com/liberit/ll-research-archive/-/raw/master/llresearch-1972-2020.txt)

Also have selections of printable signature form books here:

To print them, either print double sided long edge, or first print the even pages, then put them in the printer again, and then print the odd pages. 
I recommend you figure out how to get it to work in your printer with 1 page, and then with 1 signature (six sheets), and then you can print multiple signatures at a time.

Buddhist themese LLResearch material is at
[buddhism-book.pdf](https://gitlab.com/liberit/ll-research-archive/-/raw/master/buddhism/buddhism-book.pdf)

Hatonn (4th density) is at
[hatonn-book.pdf](https://gitlab.com/liberit/ll-research-archive/-/raw/master/hatonn/hatonn-book.pdf)

Latwii (5th density) is at
[latwii-book.pdf](https://gitlab.com/liberit/ll-research-archive/-/raw/master/latwii/latwii-book.pdf)

Ra (6th-density) is at
[ra-book.pdf](https://gitlab.com/liberit/ll-research-archive/-/raw/master/ra/ra-book.pdf)
[ramaterial-book.pdf](https://gitlab.com/liberit/ll-research-archive/-/raw/master/ramaterial/ramaterial-book.pdf)

Oxal (5th-density) is at
[oxal-book.pdf](https://gitlab.com/liberit/ll-research-archive/-/raw/master/oxal/oxal-book.pdf)

Service to Self is at
[servicetoself-book.pdf](https://gitlab.com/liberit/ll-research-archive/-/raw/master/servicetoself/servicetoself-book.pdf)

The purpose of having this archive is to be of service to our future selves and
other selves maintaining a record of this momentous work. 

> LL Research Session: March 30, 1980

> At the beginning of the next cycle there will be a breakdown of your government as you know it in the third density, and a type of dark ages, shall we say, as it may well be called in the future, will descend upon your peoples due to the fact that you have built a very fragile society in your, shall we say, civilized culture. Things will become more simple, and it is only a matter of conjecture as to what the free choice of those entities at the time will choose. However, in the fourth density there is no need for government as you know it, for the rule is freedom in love, which means that no one obstructs the freedom of another, nor would wish to harm another, and since that which is needed can be created by thought, there is no need for the labor on a cooperative basis of your society.

> However, in order for you to help, your work will be of a collective nature, melding and becoming one with each other so that you may, as a group consciousness, work for the good of the planet which nurtured you and which you now wish to nurture. Therefore, although you will not have government, you will be working as a unit in attempting to grow ever closer together.

> Hatonn, Confederation of Planets

## Archiving

For archival purposes I recommend printing single sided on acid free pages using
a laser printer with carbon black toner,
and storing in a secure cool dark place which has humidity between 20-60%. 

From my research it seems the Brother printers are the most reliable, and I've
tested that this print job works with the Brother HL-L2320D using the official
drivers.

### Time Capsule

If you live in an arid region then can easily make a timecapsule using acid free
pages that may last hundreds or thousands of years, especially in a cave or a
burried water-repellant box. Might want to put it near a landmark of some kind,
or a stone slab with an inscription.

For wetter regions, probably best would be a cave or cavern, or in a
water-resistant box burried near the top of a hill (far from the water-table or
flood plains). 

It is also possible to laser print on plastic or metal sheets. Some archivists
even print on ceramic tiles, but that gets pricey and heavy. Oldest records we
have are Sumerian fired clay tablets, and some metal tablets from the time of Jesus
([link](https://www.ancient-origins.net/news-history-archaeology/set-70-metal-tablets-may-have-earliest-written-account-and-depiction-jesus-021091)).
Though yeah there is water resistant paper available which should work, 
they say plastic can last a long time, especially if kept in the dark. 


## Personal/Future Archiving

If you'd like to convert other files from .txt to ps or pdf on Linux
you can use
[txtToPsPDF.sh](https://gitlab.com/liberit/ll-research-archive/-/raw/master/txtToPsPdf.sh?inline=false)

If you'd like to download transcripts for a year and make an HTML file out of
them, you can use [transcriptMenuURLtoHTML.sh](https://gitlab.com/liberit/ll-research-archive/-/raw/master/transcripts/transcriptMenuURLtoHTML.sh?inline=false) which is in the transcript/ folder.

### Printing Errors

I've experienced some inexplicable printing errors on some sheets, like on 
sheet 311 and 347 I was forced to remove the title of the page to get it to
print properly. The ps files are hand editable in any text editor (I use nvim).  

Though for a first run, I simply wrote down the pages that didn't print 
properly, and then continued to print whichever ones did print properly. 
And then can come back to the hard ones, printed each to its own .ps file
and got them to print however I could.  Possibly having to remove some lines.

You can submit an
[issue](https://gitlab.com/liberit/ll-research-archive/-/issues) if you are
having trouble getting it printed. 

I've also included an adjusted version that works on my printer.

## Sources 
Here are where you can get the original L/L Research archives and transcripts,
also you can give L/L Research a donation from their official page:

 * [L/L Research Channeling Archives 1972-2010](https://llresearch.org/library/channeling_archives/channeling_archives.aspx)
 * [L/L Research Transcripts 1972-2020](https://www.llresearch.org/transcripts/)
